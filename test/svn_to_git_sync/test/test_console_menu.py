import unittest
from svn_to_git_sync.console_menu import MenuConfig
import sys

class TestMenuConfig(unittest.TestCase):

    def setUp(self) -> None:
        sys.argv = sys.argv[:1]

    def test_interactive_short(self):
        sys.argv.append('-i')
        MenuConfig.init()
        self.assertTrue(MenuConfig.args.interactive)

    def test_interactive_long(self):
        sys.argv.append('--interactive')
        MenuConfig.init()
        self.assertTrue(MenuConfig.args.interactive)

    def test_map_authors(self):
        file_path = 'myfile.yaml'
        for arg in ['--map-authors', file_path]:
            sys.argv.append(arg)
        MenuConfig.init()
        self.assertEqual(file_path, MenuConfig.args.map_authors)

    def test_verbose_default(self):
        self.assertEqual(0, MenuConfig.args.verbose)

    def test_verbose_short(self):
        sys.argv.append('-v')
        MenuConfig.init()
        self.assertEqual(1, MenuConfig.args.verbose)

    def test_verbose_short_multi(self):
        sys.argv.append('-vv')
        MenuConfig.init()
        self.assertEqual(2, MenuConfig.args.verbose)

    def test_verbose_long(self):
        sys.argv.append('--verbose')
        MenuConfig.init()
        self.assertEqual(1, MenuConfig.args.verbose)

    def test_verbose_long_multi(self):
        for arg in ['--verbose', '--verbose']:
            sys.argv.append(arg)
        MenuConfig.init()
        self.assertEqual(2, MenuConfig.args.verbose)
