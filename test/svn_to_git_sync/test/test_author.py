import unittest
from svn_to_git_sync.author import Author


class TestAuthor(unittest.TestCase):
    def test_constructor(self):
        name = 'Name'
        email = 'Mail'
        instance = Author(name=name, email=email)
        self.assertEqual(instance.name, name)
        self.assertEqual(instance.email, email)
