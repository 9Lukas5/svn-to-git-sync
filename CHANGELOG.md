# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [0.6.0](../../compare/0.5.0...0.6.0) (2023-12-07)


### Features

* add option to fail if a mapping author is missing outside interactive mode ([1af6a0f](1af6a0f57609e5aae3818164eb94fca775575462))

## [0.5.0](../../compare/0.4.0...0.5.0) (2023-11-23)


### Features

* in interactive & map-authors mode: save authors on each new entry immediately ([0a07dff](0a07dff224a9e8ce1b99c707a103ef36474ef09b))

## [0.4.0](../../compare/0.3.0...0.4.0) (2023-11-22)


### Features

* add option to force set last synced revision ([0ef2342](0ef2342bc6c77c68e5a6c1556fca1921d9e32a25))


### Bug Fixes

* don't crash on between revision the target subpath was not changed ([b6cea7d](b6cea7d435f5e30c0114c7aecf0feb9bcdf57a5f))

## [0.3.0](../../compare/0.2.1...0.3.0) (2023-11-16)


### Features

* add logging ([bedaad2](bedaad2607caf96c6d2a4a9a407ee423408cea48))

## [0.2.1](../../compare/0.2.0...0.2.1) (2023-11-13)

## [0.2.0](../../compare/0.1.0...0.2.0) (2023-11-12)


### Features

* add CLI-menu and authors mapping ([4e615eb](4e615ebcd35f4e80785b0a63ecbe8ed5ee4230ad))

## 0.1.0 (2023-11-11)

First release
