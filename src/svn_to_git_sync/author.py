import yaml

from . utils import general_class_rep


class Author(yaml.YAMLObject):
    yaml_tag = u'!Author'
    yaml_loader = yaml.SafeLoader

    def __init__(
        self,
        name: str = None,
        email: str = None,
        **kwargs,                   # ignore away the ones we don't know
    ):
        self.name = name
        self.email = email

    def __repr__(self):
        return general_class_rep(self)

    @classmethod
    def yaml_representer(cls, dumper: yaml.Dumper, data):
        return dumper.represent_yaml_object(cls.yaml_tag, data, cls)

    @classmethod
    def yaml_constructor(cls, loader: yaml.SafeLoader, node):
        return cls(**loader.construct_mapping(node))


yaml.add_representer(Author, Author.yaml_representer)
yaml.add_constructor(Author.yaml_tag, Author.yaml_constructor, Author.yaml_loader)
