import logging
import inspect
from signal import signal, SIGINT, SIGTERM


class Terminator:
    interrupt = False
    __initialized = False
    __logger = logging.getLogger(__package__)

    @classmethod
    def init(cls):
        if not cls.__initialized:
            cls.__logger.log(CustomLogLevels.DEBUG_4V, 'init terminator')
            signal(SIGINT, cls.setInterrupt)
            signal(SIGTERM, cls.setInterrupt)
            cls.__initialized = True

    @classmethod
    def setInterrupt(cls, *args):
        cls.interrupt = True
        cls.__logger.log(logging.DEBUG, 'interrupt set!')


class CustomLogLevels:

    DEBUG_3V = 9
    DEBUG_4V = 8
    DEBUG_5V = 7

    @classmethod
    def init(cls):
        field: str
        for field in cls.__dict__:
            if field.startswith('DEBUG'):
                logging.addLevelName(cls.__dict__.get(field), field)


def general_class_rep(instance):
    args = '%s'
    for arg in inspect.getfullargspec(instance.__init__).args[1:]:
        args = args % (f'%s=%r, %%s' % (arg, instance.__getattribute__(arg)))
    args = args[:-4]

    fst = f'%s(%s)' % (instance.__class__.__name__, args)
    return fst


class SvnToGitSyncException(Exception):
    pass
