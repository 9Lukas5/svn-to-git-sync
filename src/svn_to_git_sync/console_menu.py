import argparse


class MenuConfig:
    args = None

    @classmethod
    def init(cls):
        mainParser = argparse.ArgumentParser(
            prog="svn-to-git-sync"
        )

        mainParser.add_argument(
            '--fail-on-missing-author',
            action='store_true',
            default=False,
            help='Causes an exception, if --map-authors is used without --interactive '
                 'and an unknown author is encountered',
        )

        mainParser.add_argument(
            '--force-last-revision',
            action='store',
            type=int,
            help='Manually set the last synced revision.'
                 'If starting to sync a subfolder of a SVN repo, that is NOT found in r0 yet,'
                 'this can be used to set the last synced revision to the one'
                 '_prior_ to the first this subfolder was part of.',
        )

        mainParser.add_argument(
            '--map-authors',
            action='store',
            help="Path to authors Mapping file."
                 "If --interactive is given, for unknown authors the user get's asked for manual input."
        )

        mainParser.add_argument(
            '-i',
            '--interactive',
            action='count',
            default=0,
            help='runs sync in interactive mode. Giving it twice, will ask on each revision for confirmation.'
        )

        mainParser.add_argument(
            '-v',
            '--verbose',
            action='count',
            default=0,
            help='Increases Logging level. Default is logging.WARNING'
        )

        cls.args = mainParser.parse_args()
