import logging

from . console_menu import MenuConfig
from . utils import Terminator, CustomLogLevels as log_lvl
from . sync import sync


def main():
    MenuConfig.init()

    log_lvl.init()
    logging.basicConfig(format='%(asctime)s [%(name)s.%(funcName)s][%(levelname)s] %(message)s')
    logger = logging.getLogger(__package__)
    switcher = {
        0: logging.WARNING,
        1: logging.INFO,
        2: logging.DEBUG,
        3: log_lvl.DEBUG_3V,
        4: log_lvl.DEBUG_4V,
        5: log_lvl.DEBUG_5V,
    }
    logger.setLevel(switcher.get(MenuConfig.args.verbose, log_lvl.DEBUG_5V))

    logger.log(logging.INFO, f'%s started', __package__)
    logger.log(logging.INFO, f'logging level: %s' % logging.getLevelName(logger.level))

    Terminator.init()

    sync()


if __name__ == '__main__':
    main()
