import logging
import os.path
import re

import git
import svn.local, svn.exception
import yaml

from . console_menu import MenuConfig
from . utils import Terminator, CustomLogLevels as log_lvl, SvnToGitSyncException
from . author import Author


def sync():
    logger = logging.getLogger(__package__)

    wc = os.path.abspath(os.path.curdir)
    logger.log(logging.INFO, f'sync called on directory: %s' % wc)

    if MenuConfig.args.interactive > 0:
        logger.log(logging.INFO, 'interactive mode!')
        print("Path to sync: %s\nContinue?(y/N) " % wc, end='')
        answer = input()
        if answer not in ['y', 'Y']:
            return

    if Terminator.interrupt:
        logger.log(logging.INFO, 'termination detected! Aborting...')
        return

    logger.log(logging.DEBUG, 'creating SVN local client')
    svn_client = svn.local.LocalClient(wc)
    try:
        svn_client.info()
    except svn.exception.SvnException as err:
        logger.log(logging.CRITICAL, f'subversion error: %s' % err)
        raise err

    try:
        logger.log(logging.DEBUG, 'creating GIT client')
        git_client = git.Repo(wc)
    except git.InvalidGitRepositoryError:
        logger.log(logging.INFO, 'no GIT repo recognized. Init new local GIT repo in %s' % wc)
        git_client = git.Repo.init(wc)

    logger.log(logging.DEBUG, 'Checking last svn revision synced on current git branch')
    last_synced_rev = 0
    if MenuConfig.args.force_last_revision:
        last_synced_rev = MenuConfig.args.force_last_revision
    elif git_client.head.is_valid():
        pattern = re.compile(f'^git-svn-id: %s@(\\d+) %s$' % (
            svn_client.info().get("repository_root"),
            svn_client.info().get("repository_uuid"),
        ))
        logger.log(log_lvl.DEBUG_3V, f'set regex to search in commit messages: "%s"' % pattern.pattern)

        match = None
        logger.log(log_lvl.DEBUG_3V, 'starting to iterate through the latest 50 commits')
        for c in git_client.iter_commits(git_client.head, max_count=50):
            c: git.Commit
            logger.log(log_lvl.DEBUG_4V, f'next commit: %s' % c)
            for line in c.message.split('\n'):
                logger.log(log_lvl.DEBUG_5V, f'next commit message line: %s' % line)
                match = pattern.match(line)
                if match:
                    last_synced_rev = int(match.group(1))
                    logger.log(logging.DEBUG, f'last SVN revision %d found in %s', last_synced_rev, c)
                    break
            if match:
                break
    logger.log(
        logging.WARNING if MenuConfig.args.interactive > 0 else logging.INFO,
        f'Last SVN revision synced: %d' % last_synced_rev,
    )

    if MenuConfig.args.interactive > 0:
        print('Continue?(y/N)...', end='')
        answer = input()
        if answer not in ['y', 'Y']:
            return

    if Terminator.interrupt:
        logger.log(logging.INFO, 'termination detected! Aborting...')
        return

    if MenuConfig.args.force_last_revision is None:
        logger.log(logging.INFO, f'resetting svn to r%s' % last_synced_rev)
        svn_client.update(revision=last_synced_rev)

    authors = dict()
    if MenuConfig.args.map_authors:
        file_path = os.path.abspath(MenuConfig.args.map_authors)
        logger.log(logging.INFO, f'syncing with author mapping from %s' % file_path)

        try:
            logger.log(logging.DEBUG, 'opening file read-only')
            authors_file = open(file_path, 'r')
            logger.log(logging.DEBUG, 'parsing yaml')
            authors: dict = yaml.safe_load(authors_file)
            if authors is None:
                raise yaml.reader.ReaderError(None, 0, 0, None, 'After load instance was null')
        except OSError:
            logger.log(logging.WARNING, f'could not open authors file: %s' % file_path)
        except yaml.reader.ReaderError as err:
            logger.log(logging.CRITICAL, f'could not parse authors file yaml: %s' % err.reason)
            raise err

    logger.log(logging.INFO, 'starting sync commit-by-commit')
    while True:
        last_synced_rev += 1
        logger.log(
            logging.WARNING if MenuConfig.args.interactive > 1 else logging.INFO,
            f'next revision: r%d' % last_synced_rev,
        )

        if MenuConfig.args.interactive > 1:
            print('Continue?(y/N)...', end='')
            answer = input()
            if answer not in ['y', 'Y']:
                break

        try:
            logger.log(log_lvl.DEBUG_3V, 'trying to update to next revision')
            svn_client.update(revision=last_synced_rev)
            svn_client.run_command('resolved', ['-R', '.'])
            svn_client.cleanup()
            svn_client.run_command('revert', ['-R', '.'])
            for st in svn_client.status():
                if st.type not in [4, 14]:
                    print(st)
                    raise Exception('svn status was strange and we could not recovery from it. Please check and re-run')
        except svn.exception.SvnException as err:

            known_endings = [
                'No such revision',
                'No such target revision',
            ]

            found_end = False
            for s in known_endings:
                if s in str(err):
                    found_end = True
                    break

            if not found_end:
                logger.log(logging.CRITICAL, 'unexpected Subversion error occurred!')
                raise err

            logger.log(
                logging.WARNING if MenuConfig.args.interactive > 1 else log_lvl.DEBUG_3V,
                'no next revision available',
            )
            break

        logger.log(log_lvl.DEBUG_3V, 'get svn commit details')
        svn_commit = None
        try:
            svn_log = list(svn_client.log_default(revision_from=last_synced_rev, revision_to=last_synced_rev))
            if len(svn_log) == 0:
                logger.log(
                    logging.WARNING if MenuConfig.args.interactive > 1 else logging.INFO,
                    'revision not part of this sub-path',
                )
                continue

            svn_commit = svn_log[0]
            if svn_commit.revision != last_synced_rev:
                raise ValueError(
                    f'expected different revision number: %s but got: %s' % (
                        last_synced_rev,
                        svn_commit.revision,
                    )
                )
        except AttributeError as err:
            logger.log(
                logging.CRITICAL,
                'Issue in dependency. See GitHub Issue: https://github.com/dsoprea/PySvn/issues/175'
            )
            logger.log(
                logging.CRITICAL,
                'temporary fix: open "lib/python3.<version>/site-packages/svn/common.py:261" '
                    'and change `e.getChildren()` to `list(e)`'
            )
            raise err

        logger.log(log_lvl.DEBUG_3V, f'creating git actor instance, with svn author: %s' % svn_commit.author)
        git_author = git.Actor(svn_commit.author, '')

        if MenuConfig.args.map_authors:
            logger.log(log_lvl.DEBUG_3V, 'mapping author')

            if git_author.name in authors:
                mapping_author: Author = authors.get(git_author.name)
                git_author.name = mapping_author.name
                git_author.email = mapping_author.email
                logger.log(log_lvl.DEBUG_4V, f'found in author file: %s <%s>' % (git_author.name, git_author.email))
            elif MenuConfig.args.interactive > 0:
                logger.log(log_lvl.DEBUG_4V, 'svn author not in mapping, but interactive mode is set')
                print(f"couldn't find author mapping for '%s'. Please enter name and email manually." % git_author.name)
                print('full name: ', end='')
                name = input()
                print('email address: ', end='')
                email = input()

                authors[git_author.name] = Author(name, email)

                try:
                    authors_file = open(file_path, 'w')
                    yaml.dump(authors, authors_file)
                except OSError:
                    logger.log(logging.WARNING, f'could not save authors file: %s' % file_path)

                git_author.name = name
                git_author.email = email
                logger.log(log_lvl.DEBUG_4V, f'author set from manual input: %s <%s>' % (git_author.name, git_author.email))

            elif MenuConfig.args.fail_on_missing_author:
                raise SvnToGitSyncException(f'author %s not in mapping file' % git_author.name)

        if Terminator.interrupt:
            logger.log(logging.INFO, 'termination detected! Aborting...')
            break

        logger.log(log_lvl.DEBUG_3V, 'stage changes in Git')
        git_client.git.add('-A')

        logger.log(log_lvl.DEBUG_3V, 'create git commit')
        git_client.index.commit(
            message=f'%s\n\n%s' %(
                svn_commit.msg,
                f'git-svn-id: %s@%s %s' % (
                    svn_client.info().get('repository_root'),
                    last_synced_rev,
                    svn_client.info().get('repository_uuid'),
                ),
            ),
            author=git_author,
            author_date=svn_commit.date,
            commit_date=svn_commit.date
        )

    if not Terminator.interrupt:
        logger.log(logging.INFO, 'all available SVN revisions synced')

    if MenuConfig.args.map_authors and MenuConfig.args.interactive > 0:
        logger.log(logging.INFO, 'author mapping and interactive mode set. Serializing potentially added authors')

        file_path = os.path.abspath(MenuConfig.args.map_authors)
        logger.log(log_lvl.DEBUG_3V, f'writing authors to %s' % file_path)

        try:
            logger.log(log_lvl.DEBUG_3V, 'opening file overwriting')
            authors_file = open(file_path, 'w')
            logger.log(log_lvl.DEBUG_3V, 'writing yaml')
            yaml.dump(authors, authors_file)
        except OSError as err:
            logger.log(logging.CRITICAL, f'could not open authors file: %s' % file_path)
            raise err
