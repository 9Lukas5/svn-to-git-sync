import os
from setuptools import setup

dev_version = os.environ.get("DEV_VERSION")
version = dev_version if dev_version else "0.0.0"

setup(version=version)
